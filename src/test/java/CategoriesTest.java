import com.zavadmak.admin.Categories;
import com.zavadmak.admin.Login;
import com.zavadmak.admin.common.AdminMenu;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

public class CategoriesTest {

    private static final String URL = "http://wordpress/wp-admin";

    private WebDriver driver;

    private AdminMenu adminMenu;
    private Categories categories;

    @BeforeEach
    public void init() {
        driver = new ChromeDriver();
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(2));

        Login loginPage = new Login(driver);
        loginPage.typeLogin("admin")
                .typePassword("12345678")
                .clickLogIn();

        adminMenu = new AdminMenu(driver);

        adminMenu.hoverPosts()
                .clickCategories();

        categories = new Categories(driver);
    }

    @AfterEach
    public void shutDown() {
        driver.quit();
    }

    @ParameterizedTest
    @Order(1)
    @CsvSource({"Temp Category, temp_category", "Temp Category 2, temp_category_2"})
    public void addNotExistsCategory(String name, String slug) {
        categories.typeName(name)
                .typeSlug(slug)
                .clickAddNewCategory();

        String message = categories.getMessage();
        String expected = "Category added.";
        assertEquals(expected, message);
    }

    @ParameterizedTest
    @Order(2)
    @CsvSource({"Temp Category, temp_category", "Temp Category 2, temp_category_2"})
    public void addAlreadyExistsCategory(String name, String slug) {
        categories.typeName(name)
                .typeSlug(slug)
                .clickAddNewCategory();

        String message = categories.getErrorMessage();
        String expected = "A term with the name provided already exists with this parent.";
        assertEquals(expected, message);
    }

    @ParameterizedTest
    @Order(3)
    @CsvSource({"temp_category", "temp_category_2"})
    public void removeExistsCategory(String slug) throws InterruptedException {
        try {
            categories.moveToSlug(slug)
                    .clickDelete(slug)
                    .clickOk();
        } catch (Exception e) {
            fail(e.getMessage());
        }

        Thread.sleep(Duration.ofSeconds(3));

        assertFalse(categories.slugExists(slug));
    }
}
