package com.zavadmak.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.bidi.module.Input;
import org.openqa.selenium.devtools.v85.page.Page;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Profile {
    private WebDriver driver;

    @FindBy(id = "submit")
    private WebElement submitButton;

    @FindBy(id = "first_name")
    private WebElement firstName;

    @FindBy(id = "last_name")
    private WebElement lastName;

    @FindBy(id = "admin_color_fresh")
    private WebElement defaultColorScheme;

    @FindBy(id = "admin_color_light")
    private WebElement lightColorScheme;

    @FindBy(id = "admin_color_coffee")
    private WebElement coffeeColorScheme;

    @FindBy(id = "nickname")
    private WebElement nickName;

    public Profile(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void refresh() {
        PageFactory.initElements(driver, this);
    }

    public Profile submit() {
        submitButton.click();
        return this;
    }

    public Profile typeFistName(String firstName) {
        this.firstName.sendKeys(firstName);
        return this;
    }

    public Profile typeLastName(String lastName) {
        this.lastName.sendKeys(lastName);
        return this;
    }

    public String getMessage() {
        return driver.findElement(By.id("message"))
                .findElement(By.tagName("p")).getText();
    }

    public String getFirstName() {
        return firstName.getAttribute("value");
    }

    public String getLastName() {
        return lastName.getAttribute("value");
    }

    public Profile selectDefaultScheme() {
        defaultColorScheme.click();
        return this;
    }

    public Profile selectLightScheme() {
        lightColorScheme.click();
        return this;
    }

    public Profile selectCoffeeScheme() {
        coffeeColorScheme.click();
        return this;
    }

    public String getSelectedSchemeName() {
        WebElement colourPicker = driver.findElement(By.id("color-picker"));
        WebElement selected = colourPicker.findElement(By.className("selected"));
        WebElement label = selected.findElement(By.tagName("label"));
        return label.getText();
    }

    public Profile clearNickName() {
        nickName.clear();
        return this;
    }

    public Profile typeNickName(String nick) {
        nickName.sendKeys(nick);
        return this;
    }

    public String getNickName() {
        return nickName.getAttribute("value");
    }

    public Profile clearFirstName() {
        firstName.clear();
        return this;
    }

    public Profile clearLastName() {
        lastName.clear();
        return this;
    }
}
