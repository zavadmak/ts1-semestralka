package com.zavadmak.admin.helpers;

import org.openqa.selenium.WebElement;

public class EditorParagraphBlock {
    WebElement element;

    public EditorParagraphBlock(WebElement el) {
        this.element = el;
    }

    public EditorParagraphBlock typeContent(String content) {
        element.sendKeys(content);
        return this;
    }

    public EditorParagraphBlock click() {
        element.click();
        return this;
    }

    public String getText() {
        return element.getText();
    }
}
