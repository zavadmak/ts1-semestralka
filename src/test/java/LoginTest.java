import com.zavadmak.admin.Login;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

public class LoginTest {
    private final String URL = "http://wordpress/wp-admin";

    private WebDriver driver;
    private Login loginPage;

    @BeforeEach
    public void init() {
        driver = new ChromeDriver();
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
        loginPage = new Login(driver);
    }

    @AfterEach
    public void restore() {
        driver.quit();
    }

    @Test
    public void changeLanguageToEnglish() {
        String selectedLang = "English (United States)";

        loginPage.clickLanguageSwitcher()
                .selectLanguage(selectedLang)
                .clickChange();

        assertEquals(selectedLang, loginPage.getSelectedLang());
    }

    @ParameterizedTest
    @CsvSource({"temp_sub, tZXkj26aW21HOuQElJ2QRVvl"})
    public void loginSubscriber(String username, String pass) {
        String selectedLang = "English (United States)";
        loginPage.clickLanguageSwitcher()
                .selectLanguage(selectedLang)
                .clickChange();

        if (!selectedLang.equals(loginPage.getSelectedLang())) {
            fail("Expected lang is not actual");
        }

        loginPage.typeLogin(username)
                .typePassword(pass)
                .clickLogIn();

        assertEquals(URL + "/profile.php", driver.getCurrentUrl());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "login_registered_admin.csv", numLinesToSkip = 1, delimiterString = ";")
    public void loginAdministrator(String username, String pass) {
        String selectedLang = "English (United States)";
        loginPage.clickLanguageSwitcher()
                .selectLanguage(selectedLang)
                .clickChange();

        if (!selectedLang.equals(loginPage.getSelectedLang())) {
            fail("Expected lang is not actual");
        }

        loginPage.typeLogin(username)
                .typePassword(pass)
                .clickLogIn();

        assertEquals(URL + "/", driver.getCurrentUrl());
    }

    @ParameterizedTest
    @CsvSource({"not_exists, 12345678"})
    public void loginNotRegisteredUser(String username, String pass) {
        String selectedLang = "English (United States)";
        loginPage.clickLanguageSwitcher()
                .selectLanguage(selectedLang)
                .clickChange();

        if (!selectedLang.equals(loginPage.getSelectedLang())) {
            fail("Expected lang is not actual");
        }

        loginPage.typeLogin(username)
                .typePassword(pass)
                .clickLogIn();
        String errorMessage = loginPage.getErrorMessage();
        String expected = "Error: The username " + username + " is not registered on this site. " +
                "If you are unsure of your username, try your email address instead.";

        assertEquals(expected, errorMessage);
    }

    @ParameterizedTest
    @CsvSource({"temp_admin, 12345678"})
    public void loginWithIncorrectPass(String username, String pass) {
        String selectedLang = "English (United States)";
        loginPage.clickLanguageSwitcher()
                .selectLanguage(selectedLang)
                .clickChange();

        if (!selectedLang.equals(loginPage.getSelectedLang())) {
            fail("Expected lang is not actual");
        }

        loginPage.typeLogin(username)
                .typePassword(pass)
                .clickLogIn();

        String errorMessage = loginPage.getErrorMessage();
        String expected = "Error: The password you entered " +
                "for the username " + username + " is incorrect. Lost your password?";

        assertEquals(expected, errorMessage);
    }
}