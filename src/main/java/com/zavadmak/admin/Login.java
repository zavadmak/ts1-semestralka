package com.zavadmak.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class Login {
    private WebDriver driver;

    @FindBy(id = "user_login")
    private WebElement loginField;

    @FindBy(id = "user_pass")
    private WebElement passwordField;

    @FindBy(id = "rememberme")
    private WebElement rememberMe;

    @FindBy(id = "wp-submit")
    private WebElement submitButton;

    @FindBy(id = "language-switcher-locales")
    private WebElement languageSwitcher;

    @FindBy(css = "#language-switcher>input.button")
    private WebElement changeLang;

    public Login(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Login typeLogin(String login) {
        this.loginField.sendKeys(login);
        return this;
    }

    public Login typePassword(String password) {
        this.passwordField.sendKeys(password);
        return this;
    }

    public Login clickRememberMe() {
        this.rememberMe.click();
        return this;
    }

    public Login clickLogIn() {
        this.submitButton.click();
        return this;
    }

    public Login clickLanguageSwitcher() {
        this.languageSwitcher.click();
        return this;
    }

    public Login selectLanguage(String lang) {
        Select langSwitcher = new Select(languageSwitcher);
        langSwitcher.selectByVisibleText(lang);
        return this;
    }

    public Login clickChange() {
        this.changeLang.click();
        return this;
    }

    public String getErrorMessage() {
        WebElement error = driver.findElement(By.id("login_error"));
        return error.getText();
    }

    public String getMessage() {
        WebElement message = driver.findElement(By.id("login-message"));
        return message.getText();
    }

    public String getSelectedLang() {
        Select s = new Select(languageSwitcher);
        return s.getFirstSelectedOption().getText();
    }
}
