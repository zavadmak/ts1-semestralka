package com.zavadmak.admin.helpers;

import com.zavadmak.admin.Editor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class EditorIframe {
    private final WebElement iframe;

    private WebElement blockPostTitle;

    private List<EditorParagraphBlock> paragraphBlocks = new ArrayList<>();

    public EditorIframe(WebElement iframe) {
        this.iframe = iframe;

        blockPostTitle = iframe.findElement(By.cssSelector("h1.wp-block-post-title"));
        WebElement blockContainer = iframe.findElement(By.cssSelector("div.wp-block-post-content"));
        List<WebElement> blocks = blockContainer.findElements(By.tagName("p"));
        blocks.forEach((el) -> {
            paragraphBlocks.add(new EditorParagraphBlock(el));
        });
    }

    public WebElement getIframe() {
        return iframe;
    }

    public EditorIframe typePostTitle(String title) {
        blockPostTitle.sendKeys(title);
        return this;
    }

    public EditorIframe clickPostTitle() {
        blockPostTitle.click();
        return this;
    }

    public EditorParagraphBlock getBlockByIndex(int index) {
        if (index > paragraphBlocks.size() - 1 || index < 0)
            return null;
        return paragraphBlocks.get(index);
    }

    public void addParagraph(String content) {

    }

    public void addNewParagraphBlock(String content) {

    }

    public String getPostTitle() {
        return blockPostTitle.getText();
    }

    public String getFirstBlockContent() {
        return getBlockByIndex(0).getText();
    }
}
