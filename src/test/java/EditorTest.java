import com.zavadmak.admin.Editor;
import com.zavadmak.admin.Login;
import com.zavadmak.admin.common.AdminMenu;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class EditorTest {
    private WebDriver driver;

    private Editor editor;

    @BeforeEach
    public void init() {
        driver = new ChromeDriver();
        driver.get("http://wordpress/wp-admin");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));

        Login login = new Login(driver);
        login.typeLogin("admin")
                .typePassword("12345678")
                .clickLogIn();
        AdminMenu menu = new AdminMenu(driver);
        menu.hoverPosts().clickAddNewPost();
        editor = new Editor(driver);
    }

    @AfterEach
    public void shutdown() {
        driver.quit();
    }

    @ParameterizedTest
    @CsvSource({"Test post, lorem ipsum something ..."})
    public void createPost(String title, String content) {
        try {
            driver.switchTo().frame(editor.getIframe());
            editor.clickTitle().typeTitle(title);

            editor.clickFirstBlock().typeFirstBlock(content);

            editor.clickPublish();
            editor.clickInterfaceInterfacePublish();

            String actualTitle = editor.getTitle();
            String actualFirstBlockContent = editor.getFistBlockContent();

            driver.switchTo().defaultContent();

            assertEquals(title, actualTitle);
            assertEquals(content, actualFirstBlockContent);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
