import com.zavadmak.admin.Login;
import com.zavadmak.admin.Profile;
import com.zavadmak.admin.common.AdminMenu;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProfileTest {

    private static final String URL = "http://wordpress/wp-admin";

    private WebDriver driver;

    private AdminMenu adminMenu;

    @BeforeEach
    public void init() {
        driver = new ChromeDriver();
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(2));

        Login loginPage = new Login(driver);
        loginPage.typeLogin("admin")
                .typePassword("12345678")
                .clickLogIn();

        adminMenu = new AdminMenu(driver);
    }

    @AfterEach
    public void shutDown() {
        driver.quit();
    }

    @Test
    @Order(1)
    public void changeSchemeToLight() throws InterruptedException {
        adminMenu.hoverUsers()
                .clickProfile();
        Profile profile = new Profile(driver);
        profile.selectLightScheme();

        Thread.sleep(Duration.ofMillis(200));

        String actualSchemeName = profile.getSelectedSchemeName();
        assertAll(
                "Change scheme to light",
                () -> assertEquals(URL + "/profile.php", driver.getCurrentUrl()),
                () -> assertEquals("Light", actualSchemeName)
        );
    }

    @Test
    @Order(2)
    public void changeSchemeToDefault() throws InterruptedException {
        adminMenu.hoverUsers()
                .clickProfile();
        Profile profile = new Profile(driver);
        profile.selectDefaultScheme();

        Thread.sleep(Duration.ofMillis(200));

        String actualSchemeName = profile.getSelectedSchemeName();
        assertAll(
                "Change scheme to default",
                () -> assertEquals(URL + "/profile.php", driver.getCurrentUrl()),
                () -> assertEquals("Default", actualSchemeName)
        );
    }

    @ParameterizedTest
    @CsvSource({"TestFirst, TestLast"})
    public void changeFirstAndLastName(String firstName, String lastName) {
        adminMenu.hoverUsers()
                .clickProfile();

        Profile profile = new Profile(driver);
        profile.clearFirstName()
                .typeFistName(firstName)
                .clearLastName()
                .typeLastName(lastName)
                .submit();

        String actualFirstName = profile.getFirstName();
        String actualLastName = profile.getLastName();
        String message = profile.getMessage();

        assertAll(
                "Change first and last name",
                () -> assertEquals("Profile updated.", message),
                () -> assertEquals(firstName, actualFirstName),
                () -> assertEquals(lastName, actualLastName)
        );
    }

    @ParameterizedTest
    @CsvSource({"test", "admin"})
    public void changeNickName(String nickname) {
        adminMenu.hoverUsers()
                .clickProfile();

        Profile profile = new Profile(driver);
        profile.clearNickName()
                .typeNickName(nickname)
                .submit();

        String actualNickName = profile.getNickName();
        String message = profile.getMessage();

        assertAll(
                "Change first and last name",
                () -> assertEquals("Profile updated.", message),
                () -> assertEquals(nickname, actualNickName)
        );
    }
}
