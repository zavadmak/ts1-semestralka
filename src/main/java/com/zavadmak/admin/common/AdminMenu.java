package com.zavadmak.admin.common;

import org.checkerframework.checker.units.qual.A;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminMenu {
    private WebDriver driver;

    private Actions actions;

    @FindBy(id = "adminmenu")
    private WebElement root;

    @FindBy(id = "menu-posts")
    private WebElement menuPosts;

    @FindBy(id = "menu-media")
    private WebElement menuMedia;

    @FindBy(id = "menu-pages")
    private WebElement menuPages;

    @FindBy(id = "menu-comments")
    private WebElement menuComments;

    @FindBy(id = "menu-users")
    private WebElement menuUsers;

    @FindBy(id = "menu-settings")
    private WebElement menuSettings;

    public AdminMenu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        actions = new Actions(driver);
    }

    public AdminMenu hoverPosts() {
        actions.moveToElement(menuPosts)
                .perform();
        return this;
    }

    public void clickPosts() {
        menuPosts.click();
    }

    public void clickAllPosts() {
        WebElement link = menuPosts.findElement(By.cssSelector("a[href='edit.php']"));
        link.click();
    }

    public void clickCategories() {
        WebElement link = menuPosts.findElement(By.cssSelector("ul>li>a[href='edit-tags.php?taxonomy=category']"));
        link.click();
    }

    public void clickTags() {
        WebElement link = menuPosts.findElement(By.cssSelector("ul>li>a[href='edit-tags.php?taxonomy=post_tag']"));
        link.click();
    }

    public void clickAddNewPost() {
        WebElement link = menuPosts.findElement(By.cssSelector("ul>li>a[href='post-new.php']"));
        link.click();
    }

    public AdminMenu hoverUsers() {
        actions.moveToElement(menuUsers)
                .perform();
        return this;
    }

    public void clickProfile() {
        WebElement link = menuUsers.findElement(By.cssSelector("ul>li>a[href='profile.php']"));
        link.click();
    }
}
