package com.zavadmak.admin;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.time.Duration;
import java.util.List;

public class Categories {

    private WebDriver driver;

    Actions actions;

    @FindBy(id = "tag-name")
    private WebElement newCategoryName;

    @FindBy(id = "tag-slug")
    private WebElement newCategorySlug;

    @FindBy(id = "parent")
    private WebElement parent;

    @FindBy(id = "submit")
    private WebElement addNewCategory;

    @FindBy(id = "the-list")
    private WebElement theListOfCategories;

    public Categories(WebDriver driver) {
        this.driver = driver;
        this.actions = new Actions(driver);
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
    }

    public Categories typeName(String name) {
        newCategoryName.sendKeys(name);
        return this;
    }

    public Categories typeSlug(String slug) {
        newCategorySlug.sendKeys(slug);
        return this;
    }

    public Categories clickParent() {
        parent.click();
        return this;
    }

    public Categories selectParent(String visibleText) {
        Select s = new Select(parent);
        s.selectByVisibleText(visibleText);
        return this;
    }

    public void clickAddNewCategory() {
        addNewCategory.click();
    }

    public String getErrorMessage() {
        try {
            WebElement ajaxResponse = driver.findElement(By.id("ajax-response"));
            WebElement notice = ajaxResponse.findElement(By.cssSelector(".notice.notice-error>p"));
            return notice.getText();
        } catch (Exception e) {
            return null;
        }
    }

    public String getMessage() {
        try {
            WebElement ajaxResponse = driver.findElement(By.id("ajax-response"));
            WebElement notice = ajaxResponse.findElement(By.cssSelector(".notice.notice-success>p"));
            return notice.getText();
        } catch (Exception e) {
            return null;
        }
    }

    public Categories clickDelete(String slug) {
        List<WebElement> categories = theListOfCategories.findElements(By.cssSelector("tr"));
        for (WebElement el: categories) {
            WebElement slugEl = el.findElement(By.cssSelector("td.slug"));
            if (slugEl.getText().equals(slug)) {
                WebElement deleteEl = el.findElement(By.cssSelector("td.name>div.row-actions>span.delete"));
                deleteEl.click();
                break;
            }
        }
        return this;
    }

    public void clickOk() {
        Alert al = driver.switchTo().alert();
        al.accept();
    }

    public Categories moveToSlug(String slug) {
        List<WebElement> categories = theListOfCategories.findElements(By.cssSelector("tr"));
        for (WebElement currentEl : categories) {
            WebElement slugEl = currentEl.findElement(By.cssSelector("td.slug"));
            if (slugEl.getText().equals(slug)) {
                actions.moveToElement(currentEl)
                        .perform();
            }
        }
        return this;
    }

    public boolean slugExists(String slug) {
        List<WebElement> categories = theListOfCategories.findElements(By.cssSelector("tr"));
        for (WebElement currentEl : categories) {
            WebElement slugEl = currentEl.findElement(By.cssSelector("td.slug"));
            if (slugEl.getText().equals(slug)) {
                return true;
            }
        }
        return false;
    }
}
