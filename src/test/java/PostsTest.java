import com.zavadmak.admin.Login;
import com.zavadmak.admin.common.AdminMenu;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PostsTest {

    private static final String URL = "http://wordpress/wp-admin";

    private WebDriver driver;

    private AdminMenu adminMenu;

    @BeforeEach
    public void init() {
        driver = new ChromeDriver();
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(2));

        Login loginPage = new Login(driver);
        loginPage.typeLogin("admin")
                .typePassword("12345678")
                .clickLogIn();

        adminMenu = new AdminMenu(driver);
    }

    @AfterEach
    public void shutDown() {
        driver.quit();
    }

    @Test
    public void showPosts() {
        adminMenu.hoverPosts()
                .clickAllPosts();

        String expected = URL + "/edit.php";
        assertEquals(expected, driver.getCurrentUrl());
    }

    @Test
    public void showCategories() {
        adminMenu.hoverPosts()
                .clickCategories();
        String expected = URL + "/edit-tags.php?taxonomy=category";
        assertEquals(expected, driver.getCurrentUrl());
    }

    @Test
    public void showTags() {
        adminMenu.hoverPosts()
                .clickTags();
        String expected = URL + "/edit-tags.php?taxonomy=post_tag";
        assertEquals(expected, driver.getCurrentUrl());
    }
}
