package com.zavadmak.admin;

import com.zavadmak.admin.helpers.EditorIframe;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Editor {

    private WebDriver driver;

    @FindBy(id = ":r0:")
    private WebElement showBlocks;

    @FindBy(id = "tabs-0-edit-post/document")
    private WebElement componentPanelPostTab;

    @FindBy(css = ".edit-post-head>.edit-post-header__settings>.editor-post-publish-button__button")
    private WebElement publishButton;

    private final EditorIframe editorIframe;

    public Editor(WebDriver driver) {
        this.driver = driver;
        editorIframe = new EditorIframe(
                driver.findElement(By.className("editor-canvas__iframe")));
        PageFactory.initElements(driver, this);
    }

    public WebElement getIframe() {
        return editorIframe.getIframe();
    }

    public Editor typeTitle(String content) {
        editorIframe.typePostTitle(content);
        return this;
    }

    public Editor clickTitle() {
        editorIframe.clickPostTitle();
        return this;
    }

    public Editor typeFirstBlock(String content) {
        editorIframe.getBlockByIndex(0)
                .typeContent(content);
        return this;
    }

    public Editor clickFirstBlock() {
        editorIframe.getBlockByIndex(0)
                .click();
        return this;
    }

    public Editor clickPublish() {
        publishButton.click();
        return this;
    }

    public Editor clickInterfaceInterfacePublish() {
        WebElement interfaceSkeletonBody = driver.findElement(By.cssSelector(".interface-interface-skeleton__body"));
        WebElement editorPublishInterfaceRegion = interfaceSkeletonBody.findElement(By.cssSelector("div[aria-label='Editor publish']"));
        WebElement confirmPublishButton = editorPublishInterfaceRegion.findElement(By.cssSelector(".editor-post-publish-panel__header-publish-button"));
        confirmPublishButton.click();
        return this;
    }

    public boolean isPublishButtonDisabled() {
        return publishButton.getAttribute("aria-disabled").equals("true");
    }

    public String getTitle() {
        return editorIframe.getPostTitle();
    }

    public String getFistBlockContent() {
        return editorIframe.getFirstBlockContent();
    }
}
