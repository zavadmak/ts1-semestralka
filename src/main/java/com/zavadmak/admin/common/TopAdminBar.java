package com.zavadmak.admin.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TopAdminBar {
    private WebDriver driver;
    private Actions actions;

    @FindBy(css = "#wp-admin-bar-my-account>a")
    private WebElement myAccount;

    @FindBy(css = "#wp-admin-bar-user-info>a")
    private WebElement editProfile;

    @FindBy(css = "#wp-admin-bar-logout>a")
    private WebElement logoutLink;

    public TopAdminBar(WebDriver driver) {
        this.driver = driver;
        actions = new Actions(driver);
        PageFactory.initElements(driver, this);
    }

    public void logout() {
        actions.moveToElement(myAccount);
        logoutLink.click();
    }

    public void performHoverAndGoToMyAccount() {
        actions.moveToElement(myAccount);
        editProfile.click();
    }

    public void goToMyAccount() {
        myAccount.click();
    }
}
